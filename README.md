# README #

This is a design exercise for a possible implementation of `RemoteStore` services within an Android client solution.

A [RemoteStore](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/remote_store/RemoteStore.java)
 is a simple object storage service. No object retrieval is considered in this solution. For this example two example objects, `Comment`, and `Annotation` are used.

Two `RemoteStore` implementations are provided to illustrate different types of `RemoteStore` services:

[LogOnlyRemoteStore](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/remote_store/log/LogOnlyRemoteStore.java) will simply write the object's `toString` result to Logcat.

[RestRemoteStore](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/remote_store/rest/RestRemoteStore.java), once properly implemented, would be able to send objects to a web service.

Since the `RestRemoteStore` may not accept the `Comment` and `Annotation` classes directly, and changing these objects would have consequences within the client application, we can utilize an adapter class [RestStoreCommentAdapter](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/remote_store/rest/adapter/RestStoreCommentAdapter.java), or [RestStoreAnnotationAdapter](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/remote_store/rest/adapter/RestStoreAnnotationAdapter.java), (neither of which are actually implemented here), to perform any necessary operations right at the point they are to be sent to the specific, target `RemoteStore`.

The [MainActivity](https://bitbucket.org/adamcmwilson/remotestore/src/master/app/src/main/java/com/adamw/remotestore/MainActivity.java) demonstrates how this setup can be actually used - where the system depends on the `RemoteStore` abstraction, and `Comment` and `Annotation` classes remain unaware of, and unaffected by, any replacement or potential combination of `RemoteStore`s.