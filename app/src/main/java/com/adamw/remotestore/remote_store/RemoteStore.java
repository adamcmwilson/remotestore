package com.adamw.remotestore.remote_store;

import com.adamw.remotestore.model.Annotation;
import com.adamw.remotestore.model.Comment;

/**
 * This interface might be enough to define a simple object storage service.
 * This solution assumes that no retrieval of stored objects is necessary at this time.
 * If there are many different types and this interface grows too large, an abstract type adapter
 * may provide a less rigid solution.
 *
 * See sample implementations:
 *      RestRemoteStore
 *      LogOnlyRemoteStore
 */
public interface RemoteStore {

    void store(Comment comment);

    void store(Annotation annotation);

}
