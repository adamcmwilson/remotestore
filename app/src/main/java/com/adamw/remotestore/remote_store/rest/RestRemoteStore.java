package com.adamw.remotestore.remote_store.rest;

import com.adamw.remotestore.model.Annotation;
import com.adamw.remotestore.model.Comment;
import com.adamw.remotestore.remote_store.RemoteStore;
import com.adamw.remotestore.remote_store.RemoteStoreAdapter;
import com.adamw.remotestore.remote_store.rest.adapter.RestStoreAnnotationAdapter;
import com.adamw.remotestore.remote_store.rest.adapter.RestStoreCommentAdapter;

/***
 * A RemoteStore that uses a web service somehow...
 */
public class RestRemoteStore implements RemoteStore {

    // The Rest Adapter, perhaps from Retrofit, to communicate with our RemoteStore web service:
    Object restAdapter;

    // Converting to String here, because the rules are made up, and the types don't matter:
    RemoteStoreAdapter<Comment, String>    comment_adapter;
    RemoteStoreAdapter<Annotation, String> annotation_adapter;

    public RestRemoteStore(Object restAdapter) {
        this.restAdapter = restAdapter;

        // Use these adapters to convert our existing classes into a form that
        //  can be properly consumed by this RemoteStore:
        comment_adapter = new RestStoreCommentAdapter();
        annotation_adapter = new RestStoreAnnotationAdapter();
    }

    @Override
    public void store(Comment comment) {
        store("/comment", comment_adapter.from(comment));
    }

    @Override
    public void store(Annotation annotation) {
        store("/annotation", annotation_adapter.from(annotation));
    }

    private void store(final String endpoint, final String json_request_body) {
        // TODO: PUT/POST request body to endpoint:
        // restAdapter.POST(api_token, endpoint, json_request_body, headers, ...);
    }

}
