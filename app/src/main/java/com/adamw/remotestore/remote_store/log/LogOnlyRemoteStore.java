package com.adamw.remotestore.remote_store.log;

import android.util.Log;

import com.adamw.remotestore.model.Annotation;
import com.adamw.remotestore.model.Comment;
import com.adamw.remotestore.remote_store.RemoteStore;

/**
 * A RemoteStore that simply logs the object toString
 * This may be useful for debugging.
 */
public class LogOnlyRemoteStore implements RemoteStore {

    final String TAG = "LogOnlyRemoteStore";

    @Override
    public void store(Comment comment) {
        Log.d(TAG, comment.toString());
    }

    @Override
    public void store(Annotation annotation) {
        Log.d(TAG, annotation.toString());
    }
}
