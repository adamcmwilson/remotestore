package com.adamw.remotestore.remote_store;

/**
 * Allows for storage objects to be converted in any manner specific to a remote store
 * implementation without needing changes to the object class itself..
 */
public interface RemoteStoreAdapter<T, R> {

    R from(T model);

}
