package com.adamw.remotestore;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.adamw.remotestore.model.Annotation;
import com.adamw.remotestore.model.Comment;
import com.adamw.remotestore.remote_store.RemoteStore;

public class MainActivity extends AppCompatActivity {

    RemoteStore remoteStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            remoteStore.store(new Comment("I am getting stored somewhere!"));
            remoteStore.store(new Annotation("Me too!"));

        } catch (Exception e) {
            Log.wtf("NOTE", "Of course I crashed here... there is no implementation of remote" +
                    "store provided at this time. This is a design exercise and not working " +
                    "software. If this were working software, perhaps there would be some form of " +
                    "dependency injection, which would allow me to substitute the RemoteStore " +
                    "based on any number of scenarios." +
                    "Even then there would be RuntimeExceptions that I have left behind as other " +
                    "components are not quite ready to roll out." +
                    "I would love to discus this incomplete solution with you!");
        }
    }
}
