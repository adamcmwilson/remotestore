package com.adamw.remotestore.remote_store.rest.adapter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class CommentAdapterTest {

    RestStoreCommentAdapter adapter;

    @Before
    public void setUp() throws Exception {
        adapter = new RestStoreCommentAdapter();
    }

    @Test
    public void from() throws Exception {
        assertNotNull("Valid tests should be written. This is not one...",
                adapter.from(null));
    }

}