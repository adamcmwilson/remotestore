package com.adamw.remotestore.remote_store.rest.adapter;

import com.adamw.remotestore.model.Annotation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AnnotationAdapterTest {

    RestStoreAnnotationAdapter adapter;

    @Before
    public void setUp() throws Exception {
        adapter = new RestStoreAnnotationAdapter();
    }

    @Test
    public void from() throws Exception {
        assertNotNull(
                "Adapter should return a valid object here, rather than throwing an exception.",
                adapter.from(new Annotation("TEST ANNOTATION")));
    }

}